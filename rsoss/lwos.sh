#!/usr/bin/env bash
# [====================================================================]
#
#  arch-setup.sh
#
#  Author:  Richard Schembri
#
# This is a script designed to be run on a fresh ArchLinux installation.
# This has not been tested yet as it is a work in progress.
#
# TODO:
# Add if statements
# [====================================================================]

# Synchronize repository databases and update the system's packages.
pacman -Syu

# tmux Terminal multiplexer.
pacman -S tmux

# Ruby
pacman -S ruby

# Create and manage tmux sessions easily.
gem install tmuxinator

# Command-line file browser with vi key bindings
pacman -S vifm

# Command-line youtube client
pacman -S mps-youtube

# Command-line music player
pacman -S cmus

# Yet Another Yogurt - An AUR Helper Written in Go
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..

# Command-line double-entry accounting system
pacman -S ledger

# Surf suckless browser
pacman -S surf

# A smart and user-friendly command line shell for Linux
pacman -S fish
