#!/usr/bin/env bash

#-------- Helper Functions {{{
#------------------------------------------------------

string-contains() {
	[ -z "$1" ] || { [ -z "${2##*$1*}" ] && [ -n "$2" ];};
}

function usage_option() {
	echo "		${1} ---> ${2}"
}

#-------- }}}

function usage() {
	echo "$SCRIPT_NAME"
	usage_option "h" "shows usage"
	usage_option "G" "Get device name"
	usage_option "C" "device name Contains string"
}

# Source taken from Neofetch: https://github.com/dylanaraps/neofetch

get-name() {
	if [[ -d /system/app/ && -d /system/priv-app ]]; then
		model="$(getprop ro.product.brand) $(getprop ro.product.model)"
	elif [[ -f /sys/devices/virtual/dmi/id/board_vendor ||
		-f /sys/devices/virtual/dmi/id/board_name ]]; then
		model=$(tr -d '\0' < /sys/devices/virtual/dmi/id/board_vendor)
		model+=" $(tr -d '\0' < /sys/devices/virtual/dmi/id/board_name)"

	elif [[ -f /sys/devices/virtual/dmi/id/product_name ||
		-f /sys/devices/virtual/dmi/id/product_version ]]; then
		model=$(tr -d '\0' < /sys/devices/virtual/dmi/id/product_name)
		model+=" $(tr -d '\0' < /sys/devices/virtual/dmi/id/product_version)"

	elif [[ -f /sys/firmware/devicetree/base/model ]]; then
		model=$(tr -d '\0' < /sys/firmware/devicetree/base/model)

	elif [[ -f /tmp/sysinfo/model ]]; then
		model=$(tr -d '\0' < /tmp/sysinfo/model)
	fi

    # Remove dummy OEM info.
    model=${model//To be filled by O.E.M.}
    model=${model//To Be Filled*}
    model=${model//OEM*}
    model=${model//Not Applicable}
    model=${model//System Product Name}
    model=${model//System Version}
    model=${model//Undefined}
    model=${model//Default string}
    model=${model//Not Specified}
    model=${model//Type1ProductConfigId}
    model=${model//INVALID}
    model=${model//All Series}
    model=${model//�}

	echo $model
}

optstring=":C:hG"

while getopts ${optstring} option; do
	case ${option} in
		h)	usage
			exit 2
			;;
		G)	get-name
			exit 2
			;;
#		C)	if [[ $(string-contains "$(get-name)" "${OPTARG}") ]]; then
		C)	if [[ "$(get-name)" == *"${OPTARG}"* ]]; then
				echo "yes"
			else
				echo "no"
			fi
			exit 2
			;;
		*)	get-name
			exit 2
			;;
	esac
done
