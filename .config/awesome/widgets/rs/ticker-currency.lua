--[[

     Licensed under GNU General Public License v2
      * (c) 2013,      Luca CPZ
      * (c) 2010-2012, Peter Hofmann

--]]

local wibox	= require("wibox")
local HOME	= os.getenv("HOME")
package.path = package.path .. ";" .. HOME .. ".local/lib/lua/?.lua"
local tickercurrency = "ticker-currency"

-- CPU usage
-- lain.widget.cpu

local function factory(args)
    args           = args or {}

    local witickercurrency= { core = {}, widget = args.widget or wibox.widget.textbox() }
    local timeout  = args.timeout or 2
    local settings = args.settings or function() end
	local currencies = args.currencies

	for k,v in ipairs(currencies) do
		v.from_symbol = v.from_symbol or v.from_currency
		v.to_symbol = v.to_symbol or v.to_currency
	end

    function witickercurrency.update()
		text_output = ""
		for k,v in ipairs(currencies) do
			currencies.rate = tickercurrency.GetTickerKraken(v.from_currency, v.to_currency)
			text_output = string.format("%s %s/%s%.2f", text_output,
														currencies.from_symbol,
														currencies.to_symbol,
														currencies.rate)
		end


        widget = witickercurrency.widget

        settings()
    end

    helpers.newtimer("tickercurrency", timeout, witickercurrency.update)

    return witickercurrency
end

return factory
