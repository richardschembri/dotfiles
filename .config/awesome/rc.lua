path_awm = os.getenv("HOME") .. '/.config/awesome'
path_scripts = path_awm .. '/scripts'

function os.capture(cmd, raw)
	local f = assert(io.popen(cmd, 'r'))
	local s = assert(f:read('*a'))
	f:close()
	if raw then return s end
	s = string.gsub(s, '^%s+', '')
	s = string.gsub(s, '%s+$', '')
	s = string.gsub(s, '\n\r', ' ')
	return s
end

function runscript(cmd, rawoutput)
	return os.capture(path_scripts .. '/' .. cmd, rawoutput)
end

if (runscript("device_name.sh -C 'C.H.I.P'", false) == "yes") then
	require("rc_pocketchip")
else
	require("rc_default")
end
