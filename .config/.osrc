#!/bin/sh

export KERNEL_NAME="$(uname -s)"
# find alternative apps if it is installed on your system
find_alt() { for i;do which "$i" >/dev/null && { echo "$i"; return 0;};done;return 1; }

case $KERNEL_NAME in
	Darwin)   export OSNAME=mac ;;
	SunOS)    export OSNAME=solaris ;;
	Haiku)    export OSNAME=haiku ;;
	MINIX)    export OSNAME=minix ;;
	AIX)      export OSNAME=aix ;;
	IRIX*)    export OSNAME=irix ;;
	FreeMiNT) export OSNAME=freemint ;;

	Linux|GNU*)
		if [[ -d /system/app/ && -d /system/priv-app ]]; then
			export OSNAME=android
		elif [ $(uname -r | sed -n 's/.*\( *Microsoft *\).*/\1/ip') ]; then
			# Is WSL
			export OSNAME=windows
		else
			export OSNAME=linux
			command -v lsb_release &> /dev/null \
				&& export DISTRONAME="$(lsb_release -a | grep "Dist" | awk -F':' '{print $2}' | sed -e 's/^[[:space:]]*//')"
		fi
		;;

		*BSD|DragonFly|Bitrig)
			export OSNAME=bsd
		;;

		CYGWIN*|MSYS*|MINGW*)
			export OSNAME=windows
		;;
esac

export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"                                                      

if command -v helix >/dev/null; then
	export EDITOR=$(which helix)
elif command -v hx >/dev/null; then
	export EDITOR=$(which hx)
elif command -v nvim >/dev/null; then
	export EDITOR=$(which nvim)
else
	export EDITOR=$(which nano)
fi

if command -v alacritty >/dev/null; then
	export TERMINAL="alacritty --config-file $(getconfig-alacritty)" # wezterm # 
	export TERM="alacritty"
fi
# export READER=zathura
export FILE=vifm
export SUDO_ASKPASS="$HOME/.scripts/dmenu/dmenu-pass.sh"

export SHELL=$(which zsh)
export BROWSERCLI=w3m
export DOTNET_CLI_TELEMETRY_OPTOUT=1

export DIRNOTES="${HOME}/Documents/notes"
export DIRNOTESPUB="${DIRNOTES}/notes-public"
export DIRDEV="${HOME}/Documents/dev"
export DIRBUREAU="${HOME}/Documents/Bureaucracy"

export APPSOURCES="$HOME/AppSources"


command -v bat >/dev/null && export MANPAGER="sh -c 'col -bx | bat -l man -p'" && export MANROFFOPT="-c"
[ -d "$HOME/go/bin" ] && export PATH="$PATH:$(du "$HOME/go/bin/" | cut -f2 | paste -sd ':')"
[ -d "$HOME/.cargo/bin" ] && path+=$HOME/.cargo/bin
[ -f "$HOME/.cargo/env" ] && source "$HOME/.cargo/env"
[ -f "$HOME/.ruby/" ] && export GEM_HOME="$HOME/.ruby/"
[ -f "$HOME/.ruby/bin" ] && export PATH="$PATH:$HOME/.ruby/bin"

command -v luarocks >/dev/null && eval "$(luarocks path --bin)"

case $OSNAME in
	windows)
		export READER=wslview
		export FILE_GUI="explorer.exe"
		cd ~
		# sudo systemd-tmpfiles --create
		;;
	linux)
		export READER=zathura
		export FILE_GUI=pcmanfm
		export INPUT_METHOD=fcitx
		# export BROWSER=brave-browser
		export BROWSER=$(find_alt firefox firefox-esr chromium-browser brave-browser)

		if [ ! command -v dotnet &> /dev/null ]
		then
			#unreal engine issue fix
			export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
		fi
		;;
	mac)
		export READER=open
		export FILE_GUI=open
		# Set LANG to UTF8 for cocoapods
		export LANG=en_US.UTF-8

		FLUTTER_PATH=~/SDK/flutter/bin
		[ -d "$FLUTTER_PATH" ] && export PATH="$PATH:$FLUTTER_PATH"
		;;
	android)
		export READER=termux-open
		export FILE_GUI=termux-open
		;;
esac


export OPENER=$(find_alt open wslview termux-open cygstart xdg-open exo-open gnome-open )
