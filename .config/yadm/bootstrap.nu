#!/usr/bin/env nu

#-------- Colors {{{
#------------------------------------------------------
let red = (tput setaf 1)
let green = (tput setaf 2)
let yellow = (tput setaf 3)
let cyan = (tput setaf 6)
let reset = (tput sgr0)
#}}}

# Print with colors example:
# print $"($red) foo ($cyan) foo2"

# Placeholder function, you cannot use variables in filepaths when using "source"
def trysource-rc [rcname:string] {
  let rcpath = $"~/.config/nushell/($rcname)"
  print $"Sourcing ($yellow)($rcpath)" 
  if  ($rcpath | path exists) {
    # source  $rcpath
  } else {
    print $"($red)rc not found: ($rcpath)" 
  }
}
 
source "~/.config/nushell/osrc.nu"
source "~/.config/nushell/aliasrc.nu"
source "~/.config/nushell/functionrc.nu"


# cannot encapsulate mutable variables :(
mut yadmstep = 1

def print-step [step:int, desc:string] {
  print $"\nStep ($step): Sourcing ($green)($desc)($reset)" 
	print "---------------------------------------------"
  print $mut:yadmstep
}

def print-error [desc:string] {
	print $"($red)($desc)($reset)" # "($reset)"
}

def print-mkdir [dirpath:string] {
	print $dirpath
	mkdir -p -- $dirpath
}

def installfrom-pkm [step:int] {
  open packages.toml | get system.cli.pkgs | (
    each {|e| print $e.name }
  )
}

def set-symlinks [step:int] {
	print-step $step "Settings symlinks"
  if (which bat | length) <= 0 and (which batcat | length) > 0 {
    sudo ln -s (which batcat) ~/.local/bin/bat
  }
  if (which python | length) <= 0 and (which python3 | length) > 0 {
  	sudo ln -s (which python3) /usr/local/bin/python
  }
} 

def generate-directories [step:int] {
	print-step $step "Generating directories"
	print-mkdir $env.DIRNOTES
	print-mkdir $env.APPSOURCES
}

def download-notes [step:int] {
  print-step $step "Downloading notes"
  print $"($env.)"
  if not ($"($env.DIRNOTESPUB)/.git" | path exists) {
    git clone https://gitlab.com/richardschembri/public-notes.git $env.DIRNOTESPUB
  } else {
		git -C $env.DIRNOTESPUB pull
  }
}

print $"($reset)"
print-step $yadmstep "This is a cow"
$yadmstep += 1
print-step $yadmstep "Konichiwa"
print-error "Foooooo"
