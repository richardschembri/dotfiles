#!/usr/bin/env sh
#
# RRRRRR   SSSSS   Richard Schembri
# RR   RR SS       https://gitlab.com/richardschembri
# RRRRRR   SSSSS   https://github.com/richardschembri
# RR  RR       SS
# RR   RR  SSSSS
#
# Modified from

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

for m in $(polybar --list-monitors | cut -d":" -f1); do
	WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar top -c ~/.config/polybar/config-top.ini &
	WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar bottom -c ~/.config/polybar/config-bottom.ini &
done
