#!/usr/bin/env nu

def print_info [ text: string ] {
    print $"(ansi xterm_yellow1)($text):(ansi reset)"
    
}

def main [ ] {
    $env.PASTIZZ = "tajbin u friski"

    print_info "Get Battery data"
    time ./battery_info.nu --asjson | ignore
    print ""

    print_info "Get Disk data"
    time ./disk_info.nu --asjson | ignore
    print ""

    print_info "Get RAM data"
    time ./ram_info.nu --asjson | ignore
    print ""

    print_info "Get Volume data"
    time ./volume.nu --asjson | ignore
    print ""

    print_info "Get Brightness data"
    time ./brightness.nu --get | ignore
    print ""

    print_info "Get CPU data"
    time ./cpu_info.nu --asjson | ignore
}
