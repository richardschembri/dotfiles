#!/usr/bin/env nu

let eww_config = $"($env.HOME)/.config/eww/"

def set_ewwvalue [varname:string varvalue:string] {
    eww -c $eww_config update $"($varname)=($varvalue)"
}

def set_ewwvalues [] {
    let audio_values = ($env.HOME/.config/eww/scripts/volume.nu --asjson) | from json
    set_ewwvalue "volume" $"($audio_values.volume)"
    set_ewwvalue "volume_mute" $"($audio_values.muted)"
    set_ewwvalue "volume_icon" $"($env.HOME/.config/eww/scripts/volume.nu --icon)"
}

def eww_running [] {
    (eww windows | rg \* | lines | length) > 0
}

def "main listen_volume" [] {
    (pactl subscribe | rg --line-buffered "Event 'change' on sink "
        | lines | each {|ai| set_ewwvalues } )
}

def main [] { }
