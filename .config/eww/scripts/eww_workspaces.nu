#!/usr/bin/env nu

let total_workspaces = (bspc query -D -d .local | wc -l) | into int
let icons = { focused: "" unfocused: "●" empty:"○" }
let eww_config = $"($env.HOME)/.config/eww/"

def update_ewwicon [wpi:int icon:string ] {
    eww -c $eww_config update $"wp($wpi)=($icon)"
}

def update_ewwicons [] {
    let currwp = (bspc query -D -d focused --names) | into int
    mut wpi = 0
    while $wpi < $total_workspaces { 
        if $currwp == $wpi {
            update_ewwicon $wpi $icons.focused
        } else {
            if (bspc query -N -d $wpi | wc -l | into int) > 0 {
                update_ewwicon $wpi $icons.unfocused
            } else {
                update_ewwicon $wpi $icons.empty
            }
        }
        $wpi = $wpi + 1
    }
}

def main [] {
    update_ewwicons
    (bspc subscribe desktop_focus node_add node_remove node_transfer
        | lines
        | each { |bi| 
            update_ewwicons
        }
    )
}
