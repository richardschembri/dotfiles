#!/usr/bin/env nu

# No Switch Case statements in Nushell :( ... yet?
if $nu.os-info.name == "Linux" or ($nu.os-info.name | str starts-with "GNU") {
  $env.OSNAME = "mac"
  if ('/system/app/' | path exists) and ( '/system/priv-app' | path exists) {
    $env.OSNAME = "android"
  } else if ($nu.os-info.kernel_version | str downcase | str contains "microsoft") {
    $env.OSNAME = "windows" # wsl
  } else {
    $env.OSNAME = "linux"
    if ('/etc/*-release' | path exists) {
      $env.DISTRO_NAME = (cat /etc/*-release | grep "^NAME" | awk -F'=' '{print $2}')
    }
  }
} else if ($nu.os-info.name | str ends-with "BSD") or $nu.os-info.name == "DragonFly" or $nu.os-info.name == "Bitrig" {
    $env.OSNAME = "bsd"
} else if ($nu.os-info.name | str starts-with "CYGWIN") or ($nu.os-info.name | str starts-with "MSYS") or ($nu.os-info.name | str starts-with "MINGW") {
    $env.OSNAME = "windows"
} else if $nu.os-info.name == "Darwin" {
  $env.OSNAME = "mac"
} else if $nu.os-info.name == "SunOS" {
  $env.OSNAME = "solaris"
} else if $nu.os-info.name == "Haiku" {
  $env.OSNAME = "haiku"
} else if $nu.os-info.name == "MINIX" {
  $env.OSNAME = "minix"
} else if $nu.os-info.name == "AIX" {
  $env.OSNAME = "aix"
} else if ($nu.os-info.name | str starts-with "IRIX") {
  $env.OSNAME = "irix"
} else if $nu.os-info.name == "FreeMiNT" {
  $env.OSNAME = "irix"
}

if ($env.OSNAME | str downcase) == "linux" {
	$env.READER = zathura
	$env.FILE_GUI = pcmanfm
	$env.INPUT_METHOD = fcitx5
	$env.BROWSER = firefox
} else if ($env.OSNAME | str downcase) == "windows" {
	$env.READER = wslview
	$env.FILE_GUI = "explorer.exe"
	cd ~
	# sudo systemd-tmpfiles --create
} else if ($env.OSNAME | str downcase) == "mac" {
		$env.READER = open
		$env.FILE_GUI = open
} else if ($env.OSNAME | str downcase) == "android" {
		$env.READER = termux-open
		$env.FILE_GUI = termux-open
}

def find-alt [apps] {
#  for x in [1 2 3] { print ($x * $x) }
  for app in $apps {
    if (which $app | length) > 0 { return  $app }
  }
  return ""
}

$env.OPENER = (find-alt ['open' 'wslview' 'termux-open' 'cygstart' 'xdg-open' 'exo-open' 'gnome-open'])

$env.EDITOR = (find-alt ['hx' 'helix' 'nvim' 'vim' 'vi' ])

if (which alacritty | length) > 0 {
  $env.TERMINAL = $"alacritty --config-file (getconfig-alacritty)" # wezterm # 
  $env.TERM = "alacritty"
} 

$env.FILE = "vifm"
$env.SUDO_ASKPASS = $"($env.HOME)/.scripts/dmenu/dmenu-pass.sh"

$env.SHELL = (which zsh)
$env.BROWSERCLI = "w3m"
$env.DOTNET_CLI_TELEMETRY_OPTOUT = 1


$env.DIRNOTES = $"($env.HOME)/Documents/notes"
$env.DIRNOTESPUB = $"($env.DIRNOTES)/notes-public"
$env.DIRDEV = $"($env.HOME)/Documents/dev"

$env.APPSOURCES = $"($env.HOME)/AppSources"

if (which bat | length) > 0 {
  $env.MANPAGER = "sh -c 'col -bx | bat -l man -p'"
  $env.MANROFFOPT = "-c"
}

$env.PATH = ($env.PATH | prepend (ls -a ~/.local/bin | where type == dir | get name | str join ":"))


if ($"($env.HOME)/go/bin" | path exists) {
  $env.PATH = ($env.PATH | prepend (ls -a $"($env.HOME)/go/bin" | where type == dir | get name | str join ":"))
}

if ($"($env.HOME)/.cargo/bin" | path exists) {
  $env.PATH = ($env.PATH | prepend (ls -a $"($env.HOME)/.cargo/bin" | where type == dir | get name | str join ":"))
}

if ($"($env.HOME)/.cargo/env" | path exists) {
  # Cargo env is in bash so cannot source
  # source "~/.cargo/env"
}

if ($"($env.HOME)/.ruby" | path exists) {
  $env.GEM_HOME = $"($env.HOME)/.ruby/"
}

if ($"($env.HOME)/.ruby/bin" | path exists) { 
  $env.PATH = ($env.PATH | prepend $"($env.HOME)/.ruby/bin")
}

if (which luarocks | length) > 0 {
    # command -v luarocks >/dev/null && eval "$(luarocks path --bin)"
}


if ($"($env.HOME)/AppSources/raylib" | path exists) { 
  $env.PATH = ($env.PATH | prepend $"($env.HOME)/raylib/src")
}

def isOS_SteamOS [] { return (($env.OSNAME | str downcase) == "steamos") }
