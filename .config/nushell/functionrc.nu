#!/usr/bin/env nu

def sync-all [] {
  printf "Syncing notes \n"
  git -C ~/Documents/notes/notes-public pull
  printf "Syncing yadm \n"
  yadm pull
}

def sync-status [] {
  yadm status
  git -C ~/Documents/notes/notes-public status
}

#-------- Configurations {{{
#------------------------------------------------------

# Run Commands
def cfg-rc-bashrc [] { ^$env.EDITOR ~/.bashrc }
def cfg-rc-zshrc [] { ^$env.EDITOR ~/.zshrc }
# Custom Run Commands
def cfg-rc-aliasrc [] { ^$env.EDITOR ~/.config/nushell/aliasrc.nu }
def cfg-rc-devrc [] { ^$env.EDITOR ~/.config/nushell/devrc.nu }
def cfg-rc-functionrc [] { ^$env.EDITOR ~/.config/nushell/functionrc.nu }
def cfg-rc-osrc [] { ^$env.EDITOR ~/.config/nushell/osrc.nu }

def cfg-starship [] { ^$env.EDITOR ~/.config/starship.toml }

# }}}

def fzf-env [] {
 $env | str join | fzf
}

def clone-gitlab [repopath] {
	git clone https://gitlab.com/($repopath).git
}

def clone-github [repopath] {
	git clone https://github.com/($repopath).git
}

def parse-file-ext [targetpath] {
	$targetpath | path parse | get extension
}

def parse-file-name [targetpath] {
	$targetpath | path parse | get stem
}
