#!/usr/bin/env nu

#-------- cd {{{
#------------------------------------------------------
alias cd-locabin = cd ~/.local/bin
alias cd-locashare = cd ~/.local/share
alias cd-locashare = cd ~/.local/share
alias cd-design = cd $nu.home-path/Documents/design
alias cd-dev = cd $env.DIRDEV
alias cd-docs = cd ( $nu.home-path | path join "Documents" )
alias cd-dld = cd ( $nu.home-path | path join "Downloads" )
# Change to local notes directory
alias cd-notesmy = cd ( $env.DIRNOTES )
# Change to wiki notes directory
alias cd-notespub = cd ( $env.DIRNOTESPUB )
# Change to work related notes directory
alias cd-noteswork = cd ( $env.DIRNOTES | path join "notes-work-project" )
alias cd-pictures = cd ( $nu.home-path | path join "Pictures" )
alias cd-recipes = cd ( $nu.home-path | path join "Documents/recipes" )
alias cd-screenshots = cd ( $nu.home-path | path join "Pictures/Screenshots" )
alias cd-sales = cd ( $nu.home-path | path join "Documents/sales" )
alias cd-manga = cd ( $nu.home-path | path join "Mangas" )
alias cd-work = cd ( $nu.home-path | path join "Documents/work" )
alias cd-finances = cd ( $nu.home-path | path join "Documents/Finances" )
alias cd-cfg = cd ( $nu.home-path | path join ".config/" )
alias cd-cfg-nvim = cd ( $nu.home-path | path join ".config/nvim" )
alias cd-cfg-nvim-plugins = cd ( $nu.home-path | path join ".config/nvim/lua/plugins" )
alias cd-cfg-awesomewm = cd ( $nu.home-path | path join "config/awesome" )
alias	cd-desktopfiles = cd ( $nu.home-path | path join ".local/share/applications" )
# }}}

#-------- todo {{{
#------------------------------------------------------
#alias xanna-todo = figlet "Calendar"; calcurse -Q --days 5; figlet "To Do"; todo.sh list
# }}}

#-------- pandoc {{{
#------------------------------------------------------
alias pandoc-cjk = pandoc --pdf-engine=xelatex -V CJKmainfont="IPAGothic"
# }}}

#-------- youtube-dl {{{
#------------------------------------------------------
alias yta-aac = yt-dlp --extract-audio --audio-format aac
alias yta-best = yt-dlp --extract-audio --audio-format best
alias yta-flac = yt-dlp --extract-audio --audio-format flac
alias yta-m4a = yt-dlp --extract-audio --audio-format m4a
alias yta-mp3 = yt-dlp --extract-audio --audio-format mp3
alias yta-opus = yt-dlp --extract-audio --audio-format opus
alias yta-vorbis = yt-dlp --extract-audio --audio-format vorbis
alias yta-wav = yt-dlp --extract-audio --audio-format wav
alias ytv-best = yt-dlp -f bestvideo+bestaudio
# }}}

#-------- helix {{{
#------------------------------------------------------
if (which helix | length) > 0 and not ((which hx | length) > 0) {
	alias hx = helix
}

if (which hx | length) > 0 and not ((which helix | length) > 0) {
	alias helix = hx
}
# }}}

#-------- termimage {{{
#------------------------------------------------------
if (which termimage | length) > 0 {
	alias termimage-32x = termimage -s 32x32
}
# }}}

#-------- w3m {{{
#------------------------------------------------------
if (which w3m | length) > 0 {
	alias w3m-ddg = w3m www.duckduckgo.com
}
# }}}

#-------- Windows aliases {{{
#------------------------------------------------------
if ("/mnt/c" | path exists) { alias cd-mntc = cd /mnt/c }
if ("/mnt/d" | path exists) { alias cd-mntc = cd /mnt/d }
# }}}
