#!/usr/bin/env bash

# Initialize wallpaper daemon
swww init &
# Set wallpaper 
# swww img ~/.config/awesome/themes/powerarrow-dark/wall-neon.png &

# Start network manage applet
nm-applet --indicator &

# Start bluetooth manage applet
blueman-applet &

# IME input method
fcitx5 &

# The taskbar
# waybar &
$HOME/.config/eww/ewwrc

# The notification manager
dunst
