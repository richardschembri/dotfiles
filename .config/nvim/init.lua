-- Vanilla Config
require('core.settings')
require('core.autocmd')
require('core.keymaps')
require('plugins')

---Pretty print lua table
function _G.dump(...)
    local objects = vim.tbl_map(vim.inspect, { ... })
    print(unpack(objects))
end
