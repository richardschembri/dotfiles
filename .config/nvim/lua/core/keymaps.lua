-- n: Normal mode
-- v: Visual mode
-- i: Insert mode
-- t: Terminal mode

vim.keymap.set({ 'n' }, '<C-h>', '<C-w>h', { silent = true, desc = '⬅ window left' })
vim.keymap.set({ 'n' }, '<C-l>', '<C-w>l', { silent = true, desc = '➡ window right' })
vim.keymap.set({ 'n' }, '<C-j>', '<C-w>j', { silent = true, desc = '⬇ window down' })
vim.keymap.set({ 'n' }, '<C-k>', '<C-w>k', { silent = true, desc = '⬆ window up' })

vim.keymap.set({ 'v' }, 'J', ":m '>+1<CR>gv=gv", { desc = '⬇ Shift selection down' })
vim.keymap.set({ 'v' }, 'K', ":m '<-2<CR>gv=gv", { desc = '⬆ Shift selection up' })

vim.keymap.set({ 'n' }, '<C-d>', "<C-d>zz", { desc = '⬇ Page down but keep cursor in middle' })
vim.keymap.set({ 'n' }, '<C-u>', "<C-u>zz", { desc = '⬆ Page up but keep cursor in middle' })

vim.keymap.set("x", "<leader>p", [["_dP]], { desc = 'Keep clipboard on overwrite paste'} )

vim.keymap.set({ 'n' }, 'n', "nzzzv", { desc = 'Next search but keep cursor in middle' })
vim.keymap.set({ 'n' }, 'N', "Nzzzv", { desc = 'Previous search but keep cursor in middle' })

vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], { desc = 'Replace highlighted text' })
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true, desc = 'Make current script executable' })
