-- Treesitter folds
-- vim.o.foldmethod = 'expr'
-- vim.o.foldexpr = 'nvim_treesitter#foldexpr()'
-- vim.o.foldlevelstart = 99

-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`
require('nvim-treesitter.configs').setup({
	ensure_installed = {
		-- Scripting
		'bash', 'python', 'lua',
		-- Low level
		'c', 'cpp', 'go', 'rust',
		-- Web
		'css', 'html', 'javascript', 'typescript',
		-- Data
		'json', 'toml',
		-- Documentation
		'markdown', 'markdown_inline',
		-- Vim
		'help', 'vim',
	},
	highlight = { enable = true },
	indent = { enable = true, disable = { 'python' } },
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = 'gs',
			-- NOTE: These are visual mode mappings
			node_incremental = 'gs',
			node_decremental = 'gS',
			scope_incremental = '<leader>gc',
		},
	},
	-- nvim-treesitter/nvim-treesitter-textobjects
	textobjects = {
		select = {
			enable = true,
			-- Automatically jump forward to textobj, similar to targets.vim
			lookahead = true,
			keymaps = {
			-- You can use the capture groups defined in textobjects.scm
				['aa'] = '@parameter.outer',
				['ia'] = '@parameter.inner',
				['af'] = '@function.outer',
				['if'] = '@function.inner',
				['ac'] = '@class.outer',
				['ic'] = '@class.inner',
				-- Or you can define your own textobjects like this
				-- ["iF"] = {
				--     python = "(function_definition) @function",
				--     cpp = "(function_definition) @function",
				--     c = "(function_definition) @function",
				--     java = "(method_declaration) @function",
				-- },
			},
		},
		move = {
			enable = true,
			set_jumps = true, -- whether to set jumps in the jumplist
			goto_next_start = {
				[']m'] = '@function.outer',
				[']]'] = '@class.outer',
			},
			goto_next_end = {
				[']M'] = '@function.outer',
				[']['] = '@class.outer',
			},
			goto_previous_start = {
				['[m'] = '@function.outer',
				['[['] = '@class.outer',
			},
			goto_previous_end = {
				['[M'] = '@function.outer',
				['[]'] = '@class.outer',
			},
		},
		swap = {
			enable = true,
			swap_next = {
				['<leader>a'] = '@parameter.inner',
			},
			swap_previous = {
				['<leader>A'] = '@parameter.inner',
			},
		},
	},
})
