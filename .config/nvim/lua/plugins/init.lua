-- Automatically run :PackerCompile whenever plugins.lua is updated with an autocommand:

--------------------------
-- Init Package Manager --
--------------------------
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-------------------
-- Setup Plugins --
-------------------
-- return require('packer').startup({
return require('lazy').setup({
	{
		"folke/which-key.nvim",
		config = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300
			require("which-key").setup({
				-- your configuration comes here
				-- or leave it empty to use the default settings
				-- refer to the configuration section below
			})
		end,
	},
	----------------------------------------
	-- Theme, Icons, Statusbar, Bufferbar --
	----------------------------------------
--	{ 
--		"ellisonleao/gruvbox.nvim",
--		config = function()
--			vim.o.background = "dark" -- or "light" for light mode
--			vim.cmd([[colorscheme gruvbox]])
--		end,
--	},
	{ 
		-- "hiroakis/cyberspace.vim",
		"rafamadriz/neon",
		config = function()
			vim.g.neon_style = "dark"
			vim.cmd([[colorscheme neon]]) -- cyberspace]])
		end,
	},
	-- Icon Pack + Icon Colors
	{
		'kyazdani42/nvim-web-devicons',
		config = function()
			require('nvim-web-devicons').setup()
		end,
	},

	 -- Status Bar
	{
		'nvim-lualine/lualine.nvim',
		-- after = 'Sakura.nvim',
		event = 'BufEnter',
		config = function()
			require('plugins.configs.lualine')
		end,
	},
	{ -- UI for nvim-lsp progress
		'j-hui/fidget.nvim',
		tag = 'legacy',
		dependencies = 'lualine.nvim',
		config = function()
			require('fidget').setup()
		end,
	},

	--------------------------------------------
	-- Treesitter: Syntax tree functionality --
	--------------------------------------------
	{
		'nvim-treesitter/nvim-treesitter',
		run = ":TSUpdate",
	},
	-- View treesitter information directly in Neovim!
	{ 'nvim-treesitter/playground', dependencies = 'nvim-treesitter' },
	-- Syntax aware text-objects, select, move, swap, and peek support.
	{ 'nvim-treesitter/nvim-treesitter-textobjects', dependencies = 'nvim-treesitter' },
	-- Refactor modules for nvim-treesitter
	{ 'nvim-treesitter/nvim-treesitter-refactor', dependencies = 'nvim-treesitter' },
	-- Use treesitter to autoclose and autorename html tag
	{ 'windwp/nvim-ts-autotag', dependencies = 'nvim-treesitter' },
	-- Commenting functionality
	{ 'JoosepAlviste/nvim-ts-context-commentstring', dependencies = 'nvim-treesitter' },

	--------------------------
	-- Editor UI Niceties --
	--------------------------
	-- Add indentation guide lines
	{
		'lukas-reineke/indent-blankline.nvim',
		event = 'BufRead',
		config = function()
			require('plugins.configs.indentline')
		end,
	},
	-- Highlight color values like Hex
	{
		'norcalli/nvim-colorizer.lua',
		event = 'CursorHold',
		config = function()
			require('colorizer').setup()
		end,
	},
	-- Git decorations
	{
		'lewis6991/gitsigns.nvim',
		event = 'BufRead',
		config = function()
			require('plugins.configs.gitsigns')
		end,
	},
	-- Shows the history of commits under the cursor in popup window
	{
		'rhysd/git-messenger.vim',
		event = 'BufRead',
		config = function()
			require('plugins.configs.git-messenger')
		end,
	},
	---------------------------------
	-- Navigation and Fuzzy Search --
	---------------------------------
	-- File Explorer
	{
		'nvim-tree/nvim-tree.lua',
		event = 'CursorHold',
		config = function()
			require('plugins.configs.nvim-tree')
		end,
	},
	-- Extendable fuzzy finder over lists.
	{
		'nvim-telescope/telescope.nvim',
		config = function()
			require('plugins.configs.telescope')
		end,
		dependencies = { 'nvim-lua/plenary.nvim' },
	},
	-- fzf
	{
		'nvim-telescope/telescope-fzf-native.nvim',
		run = 'make',
		cond = vim.fn.executable 'make' == 1,
	},
	-- Symbol search (Ex. emoji)
	{
		'nvim-telescope/telescope-symbols.nvim',
	},

	-- Smooth scroll
	{
		'karb94/neoscroll.nvim',
		event = 'WinScrolled',
		config = function()
			require('neoscroll').setup({ hide_cursor = false })
		end,
	},

	{ 'mbbill/undotree' },
	------------------------------
	-- Additional Editing tools --
	------------------------------
	-- Smart split and join. Ex. One liner -> Multi liner
	{
		'AndrewRadev/splitjoin.vim',
		-- NOTE: splitjoin won't work with `BufRead` event
		event = 'CursorHold',
	},

	--------------
	-- Markdown --
	--------------

	-- Preview Markdown
	{
		'ellisonleao/glow.nvim',
		commit = 'c88184a',
		config = function()
			require "plugins.configs.glow"
		end,
	},

	-- Vim Wiki
	{
		'jakewvincent/mkdnflow.nvim',
		rocks = 'luautf8',
		config = function()
			require "plugins.configs.mkdnflow"
		end,
	},

	-----------------------------------
	-- LSP, Completions and Snippets --
	-----------------------------------
	-- Language server installer
	-- Install and manage LSP servers, DAP servers, linters, and formatters.
	-- Configs for the Nvim LSP client
	{
		'neovim/nvim-lspconfig',
		-- after = 'telescope.nvim',
		-- event = 'BufRead',
		config = function()
			require('plugins.configs.lsp.lspconfigs')
		end,
		dependencies = {
			{
				-- Automatically install LSPs to stdpath for neovim
				'williamboman/mason.nvim',
				'williamboman/mason-lspconfig.nvim',
				-- Additional lua configuration, makes nvim stuff amazing
				'folke/neodev.nvim',
			},
		},
	},
	-- Completion engine. Completion sources are installed from external repositories
	-- and "sourced".
	{
		'hrsh7th/nvim-cmp',
		-- after = 'cmp_nvim_lsp',
		config = function()
			require('plugins.configs.lsp.nvim-cmp')
		end,
		dependencies = {
			'hrsh7th/cmp-nvim-lsp',
			-- nvim-cmp source for neovim's built-in language server client.
			'L3MON4D3/LuaSnip',
		},
	},
	-- luasnip completion source for nvim-cmp
	{ 'saadparwaiz1/cmp_luasnip', dependencies = 'nvim-cmp' },
	-- nvim-cmp source for filesystem paths.
	{ 'hrsh7th/cmp-path', dependencies = 'nvim-cmp' },
	-- nvim-cmp source for buffer words.
	{ 'hrsh7th/cmp-buffer', dependencies = 'nvim-cmp' },
--[[
	use({
		'Hoffs/omnisharp-extended-lsp.nvim'
	})
--]]
	-- NOTE: nvim-autopairs needs to be loaded after nvim-cmp, so that <CR> would work properly
	{
		'windwp/nvim-autopairs',
		event = 'InsertCharPre',
		dependencies = 'nvim-cmp',
		config = function()
			require('plugins.configs.pairs')
		end,
	},

	-------------
	-- Gamedev --
	-------------
	{ 'ranjithshegde/Unreal.nvim' },
	config = {
		display = {
			open_fn = function()
				return require('packer.util').float({ border = 'single' })
			end,
		},
	},
})
