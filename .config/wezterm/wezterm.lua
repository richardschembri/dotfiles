local wezterm = require 'wezterm'
local act = wezterm.action
return {
	color_scheme = "Gruvbox Dark",
	check_for_updates = false,
	show_update_window = false,
	hide_tab_bar_if_only_one_tab = false,
	adjust_window_size_when_changing_font_size = false,
	enable_scroll_bar = true,
	keys = {
		{ key = 'j', mods = 'ALT', action = act.ScrollByLine(1) },
		{ key = 'k', mods = 'ALT', action = act.ScrollByLine(-1) },
		{ key = 'j', mods = 'ALT|SHIFT', action = wezterm.action.DecreaseFontSize },
		{ key = 'k', mods = 'ALT|SHIFT', action = wezterm.action.IncreaseFontSize },
		{ key = '0', mods = 'CTRL', action = wezterm.action.ResetFontSize },
	},
}
