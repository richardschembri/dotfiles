#!/usr/bin/env nu
# The following relies on ~/.local/bin/screenshot.nu

let theme = $"($env.HOME)/.config/rofi/themes/applets.rasi"

let str_tofile = "to file "
let str_toclipboard = "to clipboard "
let str_open = "open latest  "

let str_selected_area = " Selected area"
let str_current_window = " Current window"
let str_fullscreen = " Fullscreen"


let listitems = {
	tofile: {
						selected_area: $"($str_selected_area) ($str_tofile)" 
						current_window: $"($str_current_window) ($str_tofile)" 
						fullscreen: $"($str_fullscreen) ($str_tofile)" 
					}
	toclipboard: {
						selected_area: $"($str_selected_area) ($str_toclipboard)" 
						current_window: $"($str_current_window) ($str_toclipboard)" 
						fullscreen: $"($str_fullscreen) ($str_toclipboard)" 
					}
	openimage: {
						selected_area: $"($str_selected_area) ($str_open)" 
						current_window: $"($str_current_window) ($str_open)" 
						fullscreen: $"($str_fullscreen) ($str_open)" 
					}
}

let	list_col = "1"
let	list_row = "6"
let prompt = "Screenshot"
let mesg = $"DIR: (screenshot.nu printpath)"

# Rofi CMD
def rofi_cmd [opts: string] {
	$opts | (rofi -theme-str $"listview {columns: ($list_col); lines: ($list_row);}"
		-theme-str 'textbox-prompt-colon {str: "";}'
		-dmenu
		-p $"($prompt)"
		-mesg $"($mesg)"
		-markup-rows
		-theme $"($theme)")
}

# Pass variables to rofi dmenu
def run_rofi [] {
	rofi_cmd $"($listitems.toclipboard.selected_area)\n($listitems.tofile.selected_area)\n($listitems.openimage.selected_area)\n($listitems.toclipboard.fullscreen)\n($listitems.tofile.fullscreen)\n($listitems.openimage.fullscreen)\n($listitems.tofile.current_window)\n($listitems.toclipboard.current_window)\n($listitems.openimage.current_window)"
}

def main [] {
  let chosen = run_rofi 	
	if ( $chosen == $listitems.tofile.selected_area ) {
		screenshot.nu file 0
	} else if ( $chosen == $listitems.tofile.current_window ) {
		screenshot.nu file 1
	} else if ( $chosen == $listitems.tofile.fullscreen ) {
		screenshot.nu file 2 
	} else if ( $chosen == $listitems.tofile.selected_area ) {
		screenshot.nu toclipboard 0
	} else if ( $chosen == $listitems.tofile.current_window ) {
		screenshot.nu toclipboard 1
	} else if ( $chosen == $listitems.tofile.fullscreen ) {
		screenshot.nu toclipboard 2 
	}

	# The following complains that the json variable is not an expected variable
#	match $chosen {
#		{ $listitems.tofile.selected_area } => ( screenshot.nu file 0 )
#		{ $listitems.tofile.current_window } => ( screenshot.nu file 1 )
#		{ $listitems.tofile.fullscreen } => ( screenshot.nu file 2 )
#		{ $listitems.toclipboard.selected_area } => ( screenshot.nu toclipboard 0 )
#		{ $listitems.toclipboard.current_window } => ( screenshot.nu toclipboard 1 )
#		{ $listitems.toclipboard.fullscreen } => ( screenshot.nu toclipboard 2 )
#	}
}
