#!/usr/bin/env nu

let theme = $"($env.HOME)/.config/rofi/themes/powermenu.rasi"

let li_shutdown = " Shutdown"
let li_reboot =   " Reboot"
let li_lock =     " Lock"
let li_suspend =  " Suspend"
let li_logout =   " Logout"
let li_yes =      " Yes"
let li_no =       " No"

let uptime = "to be implemented"

# Confirmation CMD
def confirm_cmd [opts: string] {
	sleep 0.5sec
	$opts | (rofi -theme-str "window {location: center; anchor: center; fullscreen: false; width: 250px;}" 
		-theme-str "mainbox {children: [ \"message\", \"listview\" ];}"
		-theme-str "listview {columns: 2; lines: 1;}"
		-theme-str "element-text {horizontal-align: 0.5;}"
		-theme-str "textbox {horizontal-align: 0.5;}"
		-dmenu
		-p "Confirmation"
		-mesg "Are you Sure?"
		-theme $"($theme)")
}

# Ask for confirmation
def confirm_exit [] {
	confirm_cmd $"($li_no)\n($li_yes)" 
}

# Pass variables to rofi dmenu
def run_rofi [] {
	($"($li_lock)\n($li_suspend)\n($li_logout)\n($li_reboot)\n($li_shutdown)"
		|	(rofi -dmenu
				-p $"(hostname)"
				-mesg $"Uptime: ($uptime)"
				-theme $"($theme)"))
}

# Execute Command
def run_cmd [
		--shutdown (-s)
		--reboot (-r)
		--suspend (-p)
		--logout (-l)
		--lock (-k)
] {
	let selected = confirm_exit
	print "say wha?"
	print $"shutdown:($shutdown) reboot:($reboot) suspend:($suspend) logout:($logout) lock:($lock)"
	if ($selected == $li_yes) {
			if ($shutdown) {
					systemctl poweroff
			} else if ($reboot) {
					systemctl reboot
			} else if ($suspend) {
					mpc -q pause
					amixer set Master mute
					systemctl suspend
			} else if ($logout) {
					match $env.DESKTOP_SESSION {
						"openbox" => (openbox --exit),
						"bspwm" => (bspc quit),
						"i3" => (i3-msg exit),
						"plasma" => (echo "not yet implemented")
					}
					#qdbus org.kde.ksmserver /KSMServer logout 0 0 0
			} else if ($lock) {
			}
	} else {
			exit 0
	}
}

def main [] {
	# Actions
	let chosen = run_rofi

	if $chosen == $li_shutdown {
		run_cmd --shutdown
	} else if $chosen == $li_reboot {
		run_cmd --reboot
	} else if $chosen == $li_lock {
		run_cmd --lock
	} else if $chosen == $li_suspend {
		run_cmd --suspend
	} else if $chosen == $li_logout {
		run_cmd --logout
	}

#	match $chosen {
#		$li_shutdown => { print $"shutdown ($chosen)"; run_cmd --shutdown }, 
#		$li_reboot => { print $"reboot ($chosen)"; run_cmd --reboot },
#		$li_lock => { print $"lock ($chosen)"; run_cmd --lock },
#		$li_logout => { run_cmd --logout }
#	}
}
