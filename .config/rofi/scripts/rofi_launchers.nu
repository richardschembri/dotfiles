#!/usr/bin/env nu

def main [
    --drun (-d)
    --run (-r)
    --filebrowser (-f)
    --window (-w)
] {
    let theme = $"($env.HOME)/.config/rofi/themes/launchers.rasi"
    if ($drun) {
        rofi -show drun -theme $theme
    } else if ($run) {
        rofi -show run -theme $theme
    } else if ($filebrowser) {
        rofi -show filebrowser -theme $theme
    } else if ($window) {
        rofi -show window -theme $theme
    } else {
        rofi -show run -theme $theme
    }
}
