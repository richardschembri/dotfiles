" File: .vimrc
"
" Author: Richard Schembri
" References: https://vimawesome.com

" Needs to be first
set nocompatible

filetype off
filetype plugin on

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"set rtp+=~/.vim/bundle/Vundle.vim
"call vundle#begin()
call plug#begin('~/.vim/plugged')

Plug 'VundleVim/Vundle.vim'

" ----- Vim Appearance ------------------------------------------------
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" - Theme -
"Plugin 'tomasiser/vim-code-dark'
Plug 'gruvbox-community/gruvbox'

" Zoom window
Plug 'troydm/zoomwintab.vim'

" Adds icons to plugins
Plug 'ryanoasis/vim-devicons'

" A plugin to color colornames and codes
Plug 'ap/vim-css-color'

" ----- Text Editor functionality -------------------------------------
"  Markdown Preview (requires instant-markdown-d to be installed via npm)
Plug 'suan/vim-instant-markdown'

" ----- Search functionality ---- -------------------------------------
" Full path fuzzy file, buffer, mru, tag, ... finder for Vim
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
" Search directories recursively with grep
Plug 'jremmen/vim-ripgrep'

" ----- IDE like functionality ----------------------------------------
" File browser pane
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
" syntax checker
"Plugin 'vim-syntastic/syntastic'
" Asynchronous syntax check
Plug 'dense-analysis/ale'
" Required for vim-easytags
" Plugin 'xolox/vim-misc'
" Plugin 'xolox/vim-easytags'
Plug 'majutsushi/tagbar'

" Code auto completion tool
" Plugin 'valloric/youcompleteme'

" Conquer of Completion
" Be sure to have npm and yarn installed
" Plug 'neoclide/coc.nvim'

" Ultisnips - Code Snippet tool
Plug 'sirver/ultisnips'

" ----- Language/Framework specific --------------------------
" A collection of language packs for Vim
Plug 'sheerun/vim-polyglot'

" Laravel support // https://vimawesome.com/plugin/vim-laravel-face-rejection
Plug 'tpope/vim-projectionist'
Plug 'noahfrederick/vim-composer'
Plug 'noahfrederick/vim-laravel'

" C Tags for PHP
Plug 'vim-php/phpctags'
" Tagbar for php
Plug 'vim-php/tagbar-phpctags.vim'

" C#
Plug 'OmniSharp/omnisharp-vim'
Plug 'OrangeT/vim-csharp'

" Unity
Plug 'kitao/unity_dict'

" Rust // :CocInstall coc-rust-analyzer
Plug 'rust-analyzer/rust-analyzer'

" ----- Debug tools --------------------------

" Step debugging for certain type of languages
Plug 'joonty/vdebug'

" ----- Syntax Highlighting --------------------------------

" Colored Parentheses
Plug 'kien/rainbow_parentheses.vim'

" Rainbow color CSV data + sql type command support
Plug 'mechatroner/rainbow_csv'

" automatic closing of quotes, parenthesis, brackets, etc.
Plug 'Raimondi/delimitMate'

" ----- Git Support -----------------------------------------
" https://github.com/airblade/vim-gitgutter
" Plugin 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" ----- Productivity ----------------------------------------
Plug 'vimwiki/vimwiki'
"
" ---- Extras/Advanced plugins ------------------------------
" Highlight and strip trailing whitespace
" Plugin 'ntpeters/vim-better-whitespace'
" Easily surround chunks of text
Plug 'tpope/vim-surround'
" Align CSV files at commas, align Markdown tables, and more
Plug 'godlygeek/tabular'

" Multi cursor // https://github.com/terryma/vim-multiple-cursors
Plug 'terryma/vim-multiple-cursors'

" Ledger support
Plug 'ledger/vim-ledger'

" Markdown Preview
Plug 'tpope/vim-markdown'

" Bookmarks https://github.com/MattesGroeger/vim-bookmarks
Plug 'MattesGroeger/vim-bookmarks'

" Visualize undo history in tree form
Plug 'mbbill/undotree'


" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'

"call vundle#end()
call plug#end()

function! GetRunningOS()
  if has("win32")
    return "win"
  endif
  if has("unix")
    if system('uname')=~'Darwin'
      return "mac"
    else
      let lines = readfile("/proc/version")
      if lines[0] =~ "Microsoft"
        return "wsl"
      else
        return "linux"
      endif
    endif
  endif
endfunction
let os=GetRunningOS()

" --- General Settings ---
" line number, the column number, the virtual column number, and the relative position
set ruler
" display line numbers in the left margin
set number
set relativenumber
" Show vim commands
set showcmd
" Jump to search item whilst typing
set incsearch
" Highlight logical search match
set hlsearch

" Better case sensitive search:
" If you search for something containing uppercase characters,
" it will do a case sensitive search;
" If you search for something purely lowercase,
" it will do a case insensitive search.
set ignorecase
set smartcase

" For DevIcons
set encoding=utf8

if os == "linux" || os == "wsl"
	set guifont=DroidSansMono\ Nerd\ Font\ 11
else
	set guifont=DroidSansMono\ Nerd\ Font:h11
	" set guifont=DroidSansMono_Nerd_Font:h11
endif

" set a map leader for more key combos
let mapleader = ' '
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

nnoremap <silent> <Leader>+ :vertical resize +5<CR>
nnoremap <silent> <Leader>- :vertical resize -5<CR>

" Turn on syntax highlighting
syntax on
" Turn on mouse support
" set mouse=a
map <F3> <ESC>:exec &mouse!=""? "set mouse=" : "set mouse=nv"<CR>

" Show a line for column limit
set colorcolumn=80

set tabstop=4 softtabstop=4
set shiftwidth=4
set smartindent

set undodir=~/.vim/undodir
nnoremap <leader>u :UndotreeToggle<CR>

" Copy to clipboard by default
if system('uname -s') == "Darwin\n"
  set clipboard=unnamed "OSX
else
  set clipboard=unnamedplus "Linux
endif

"colorscheme codedark
colorscheme gruvbox
set background=dark
"let g:gruvbox_colors = 'dark0'

set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//

" ----- vimwiki -----------------------
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
let g:vimwiki_global_ext = 0
let g:vimwiki_list = [{'path': '~/Documents/notes/notes-public', 'path_html': '~/Documents/notes    :arrow_right_hook: /notes-public-html/', 'ext': '.md'},{'path': '~/Documents/notes/notes-private', 'path_html': '~/Documents/notes/ :arrow_right_hook: notes-private-html/', 'ext': '.md'},{'path': '~/Documents/notes/notes-project', 'path_html': '~/Documents/notes/notes-project    :arrow_right_hook: /notes-projects-html/', 'ext': '.md'},{'path': '~/Documents/notes/notes-work-projects', 'path_html': '~/Documents/notes/notes-work-projects    :arrow_right_hook: /notes-work-projects-html/', 'ext': '.md'}, {'path': '~/Documents/notes/zettel-private', 'path_html': '~/Documents/notes    :arrow_right_hook: /zettle-private-html/', 'ext': '.md'}, {'path': '~/Documents/recipes', 'path_html': '~/Documents/recipes    :arrow_right_hook: /recipes-html/', 'ext': '.md'}, {'path': '~/Documents/fitness/notes-fitness', 'path_html': '~/Documents/fitness/notes-fitness    :arrow_right_hook: /notes-fitness-html/', 'ext': '.md'}, {'path': '~/Documents/notes/notes-japan', 'path_html': '~/Documents/notes/notes-japan    :arrow_right_hook: /notes/notes-japan-html/', 'ext': '.md'}]

"----- Fuzzy find search --------------
set nocompatible " Limit search to project
set path+=** "Search subdirectories recursively
set wildmenu "Show search results in one line

" ----- bling/vim-airline settings -----
" Always show statusbar
set laststatus=2

" Show airline for tabs too
let g:airline#extensions#tabline#enabled = 1
" ALE support
let g:airline#extensions#ale#enabled = 1

let g:airline_powerline_fonts = 1

"let g:airline_theme = 'codedark'
let g:airline_theme = 'gruvbox'

" ----- dense-analysis/ale ---------
let g:ale_set_balloons = 1

" ----- scrooloose/nerdtree ---------
nnoremap <leader>pv :NERDTreeToggle<CR>
" ----- jistr/vim-nerdtree-tabs -----
" Open/close NERDTree Tabs with \t
nmap <silent> <leader>t :NERDTreeTabsToggle<CR>

" ----- troydm/zoomwintab -----------
nmap <silent> <leader>m :ZoomWinTabToggle<CR>
" enable/disable zoomwintab integration >
let g:airline#extensions#zoomwintab#enabled = 1

" jremmen/vim-ripgrep
" Allows ripgrep to automatically find the git root
if executable('rg')
	let g:rg_derive_root='true'
endif

" ripgrep: Project Search
nnoremap <Leader>ps :Rg<SPACE>

" valloric/youcompleteme
" Go to Definition
" nnoremap <silent> <Leader>gd :YcmCompleter GoTo<CR>

" -----  junegunn/fzf (Fuzzy finder) -----------
nnoremap <C-p> :Files<CR>

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" To have NERDTree always open on startup
"let g:nerdtree_tabs_open_on_console_startup = 1
" ----- Mimic Nerdtree using netrw ----
" let g:netrw_banner = 1
" let g:netrw_liststyle = 3
" let g:netrw_browse_split = 4
" let g:netrw_altv = 1
" let g:netrw_winsize = 25

" " Toggle Vexplore with F2
" function! ToggleVExplorer()
"     if exists("t:expl_buf_num")
"         let expl_win_num = bufwinnr(t:expl_buf_num)
"         let cur_win_num = winnr()

"         if expl_win_num != -1
"             while expl_win_num != cur_win_num
"                 exec "wincmd w"
"                 let cur_win_num = winnr()
"             endwhile

"             close
"         endif

"         unlet t:expl_buf_num
"     else
"          Vexplore
"          let t:expl_buf_num = bufnr("%")
"     endif
" endfunction
" map <silent> <f2> :call ToggleVExplorer()<CR>

" Markdown settings
let g:markdown_fenced_languages = ['c++=cpp', 'viml=vim', 'bash=sh', 'ini=dosini', 'csharp=cs', 'solidity']
" - Markdown Preview settings
"Uncomment to override defaults:
"let g:instant_markdown_slow = 1
"let g:instant_markdown_autostart = 0
"let g:instant_markdown_open_to_the_world = 1
"let g:instant_markdown_allow_unsafe_content = 1
"let g:instant_markdown_allow_external_content = 0
"let g:instant_markdown_mathjax = 1
"let g:instant_markdown_logfile = '/tmp/instant_markdown.log'
"let g:instant_markdown_autoscroll = 0
"let g:instant_markdown_port = 8090
"let g:instant_markdown_python = 1

" Highlight trailing white spaces
highlight ExtraWhitespace ctermbg=red guibg=red
"highlight NonText ctermbg=red guibg=red
highlight SpecialKey ctermfg=236
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Visible tabs
"":set listchars=eol:⏎,tab:↹·,trail:~,nbsp:⎵,space:␣
:set listchars=eol:⏎,tab:↹·,nbsp:⎵,extends:»,precedes:«
:set list
set showbreak=↪\ 

" Rainbow Parentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" ------------- FuGITive -------------
nmap <leader>gh :diffget //3<CR>
nmap <leader>gu :diffget //2<CR>
nmap <leader>gs :G<CR>

" -------- SirVer/ultisnips ---------

" Snippets are separated from the engine. Add this if you want them:
" Plugin 'honza/vim-snippets'

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<leader><tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" ------------- CSharp --------------
"  Fold CSharp region by typing zf%
let b:match_words = '\s*#\s*region.*$:\s*#\s*endregion'

" Omnisharp
if os == "wsl"
" - Windows Subsystem for Linux (WSL)
  let g:OmniSharp_server_path = '/mnt/c/OmniSharp/omnisharp.win-x64/OmniSharp.exe'
  let g:OmniSharp_translate_cygwin_wsl = 1'
elseif os != "win"
  " - Linux/Mac
  let g:OmniSharp_server_use_mono = 1
endif

if has('patch-8.1.1880')
  set completeopt=longest,menuone,popuphidden
  " Highlight the completion documentation popup background/foreground the same as
  " the completion menu itself, for better readability with highlighted
  " documentation.
  set completepopup=highlight:Pmenu,border:off
else
  set completeopt=longest,menuone,preview
  " Set desired preview window height for viewing documentation.
  set previewheight=5
endif

" Timeout in seconds to wait for a response from the server
let g:OmniSharp_timeout = 30

let g:OmniSharp_highlight_groups = {
\ 'Comment': 'NonText',
\ 'XmlDocCommentName': 'Identifier',
\ 'XmlDocCommentText': 'NonText'
\}

let g:OmniSharp_popup_options = {
\ 'highlight': 'Normal',
\ 'padding': [1],
\ 'border': [1]
\}

" Syntastic support for Omnisharp
" let g:syntastic_cs_checkers = ['code_checker']

" ALE support for Omnisharp
let g:ale_linters = {
\ 'cs': ['OmniSharp'],
\ 'rust': ['analyzer']
\}


" fzf support for Omnisharp
let g:OmniSharp_selector_ui = 'fzf'    " Use fzf.vim

augroup omnisharp_commands
  autocmd!

  " Show type information automatically when the cursor stops moving.
  " Note that the type is echoed to the Vim command line, and will overwrite
  " any other messages in this space including e.g. ALE linting messages.
  autocmd CursorHold *.cs OmniSharpTypeLookup

  " The following commands are contextual, based on the cursor position.
  autocmd FileType cs nmap <silent> <buffer> gd <Plug>(omnisharp_go_to_definition)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfu <Plug>(omnisharp_find_usages)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfi <Plug>(omnisharp_find_implementations)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ospd <Plug>(omnisharp_preview_definition)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ospi <Plug>(omnisharp_preview_implementations)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ost <Plug>(omnisharp_type_lookup)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osd <Plug>(omnisharp_documentation)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfs <Plug>(omnisharp_find_symbol)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfx <Plug>(omnisharp_fix_usings)
  autocmd FileType cs nmap <silent> <buffer> <C-\> <Plug>(omnisharp_signature_help)
  autocmd FileType cs imap <silent> <buffer> <C-\> <Plug>(omnisharp_signature_help)

  " Navigate up and down by method/property/field
  autocmd FileType cs nmap <silent> <buffer> [[ <Plug>(omnisharp_navigate_up)
  autocmd FileType cs nmap <silent> <buffer> ]] <Plug>(omnisharp_navigate_down)
  " Find all code errors/warnings for the current solution and populate the quickfix window
  autocmd FileType cs nmap <silent> <buffer> <Leader>osgcc <Plug>(omnisharp_global_code_check)
  " Contextual code actions (uses fzf, CtrlP or unite.vim selector when available)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osca <Plug>(omnisharp_code_actions)
  autocmd FileType cs xmap <silent> <buffer> <Leader>osca <Plug>(omnisharp_code_actions)
  " Repeat the last code action performed (does not use a selector)
  autocmd FileType cs nmap <silent> <buffer> <Leader>os. <Plug>(omnisharp_code_action_repeat)
  autocmd FileType cs xmap <silent> <buffer> <Leader>os. <Plug>(omnisharp_code_action_repeat)

  autocmd FileType cs nmap <silent> <buffer> <Leader>os= <Plug>(omnisharp_code_format)

  autocmd FileType cs nmap <silent> <buffer> <Leader>osnm <Plug>(omnisharp_rename)

  autocmd FileType cs nmap <silent> <buffer> <Leader>osre <Plug>(omnisharp_restart_server)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osst <Plug>(omnisharp_start_server)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ossp <Plug>(omnisharp_stop_server)
augroup END
