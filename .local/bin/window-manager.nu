#!/usr/bin/env nu

def main [ ] { }

# Get window name
def "main name" [ ] {
	xdotool getwindowfocus getwindowname
}

# Set window to floating
def "main float" [ ] {
	bspc node focused -t "~floating"
#	if (bspc query -N -n focused.floating | length) > 0 {
#		bspc node -t tiled
#	} else {
#		bspc node -t floating
#	}
}

# Toggle maximize window
def "main maximize" [ ] {
	bspc desktop -l next
}

# Set window to sticky
def "main sticky" [ ] {
	bspc node -g sticky
}

# Close window
def "main close" [ ] {
	bspc node -c
}

def "main shift" [direction:string] {
	match ($direction) {
		"up" => (bspc node -s north)
		"down" => (bspc node -s south)
		"left" => (bspc node -s west)
		"right" => (bspc node -s east)
	}
}
