#!/usr/bin/env nu

def main [
    --used (-u)
    --all (-a)
    --parsed (-p)
    --percent (-c)
    --asjson (-j)
] {
    let total_ram = (free -t --mega | from ssv -m 1 | rename label total used free shared "buff/cache" available
                        | where label =~ "Total" | into value
                        | insert used_gb {|ram_values| $"($ram_values.used / 1024 | math round --precision 1)"}
                        | insert total_gb {|ram_values| $"($ram_values.total / 1024 | math round --precision 1)"}
                        | insert parsed {|ram_values| $"($ram_values.used_gb) / ($ram_values.total_gb) GB"}
                        | insert percent {|ram_values| (($ram_values.used / $ram_values.total) * 100)}
                        | get 0)

    if $asjson {
        $total_ram | to json
    } else if $used {
        ($total_ram.used / 1024)
        #print (free -t --mega | grep Total | awk '{printf $3/100}')
    } else if $all {
        ($total_ram.total / 1024)
        #print (free -t --mega | grep Total | awk '{printf $2/100}')
    } else if $parsed {
        print $"($total_ram.used)/($total_ram.total)"
        # print (free -t -h --si | grep Total | awk '{printf $3 "b/" $2 "b"}')
    } else if $percent {
        print $total_ram.percent
        # printf "%.0f\n" (free -m | grep Mem | awk '{print ($3/$2)*100}')
    }
}
