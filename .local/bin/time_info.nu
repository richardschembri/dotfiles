#!/usr/bin/env nu

def main [
    --asjson (-j)
    --year (-y)
    --month (-h)
    --day (-d)
    --hour (-h)
    --minute (-m)
    --second (-s)
    --nanosecond (-n)
    --timezone (-z)
    --time (-t)
    --ymd (-e)
] {

   let date_record = date now | date to-record | to json

   if ($asjson) {
        $date_record | to json
    } else if ($year) {
        $date_record.year
    } else if ($month) {
        $date_record.month
    } else if ($day) {
        $date_record.day
    } else if ($hour) {
        $date_record.hour
    } else if ($minute) {
        $date_record.minute
    } else if ($second) {
        $date_record.second
    } else if ($nanosecond) {
        $date_record.nanosecond
    } else if ($timezone) {
        $date_record.timezone
    } else if ($ymd) {
        $"($date_record.year)/($date_record.month)/($date_record.day)"
    } else if ($time) {
        $"($date_record.hour):($date_record.minute):($date_record.second)"
    }
}
