#!/usr/bin/env sh

sheets_dir="$HOME/Documents/Cheatsheets"
# cheatsheet=$( (find $sheets_dir -regex '.*\.\(txt\|md\)' | sed 's@^\./@@' | egrep -v '_files\/' | sort) | rofi -dmenu -i -hide-scrollbar -p "Select cheatsheet")
cheatsheet=$( (find $sheets_dir -regex '.*\.\(txt\|md\)' | sed 's@^\./@@' | egrep -v '_files\/' | sort) | rofi -dmenu -i -hide-scrollbar -p "Select cheatsheet")

if [ -n "${cheatsheet}" ]
then
    # cat "${sheets_dir}/${cheatsheet}" | rofi -dmenu -i -hide-scrollbar -p "Search cheatsheet"
    cat "${cheatsheet}" | rofi -dmenu -i -hide-scrollbar -p "Search cheatsheet"
fi
