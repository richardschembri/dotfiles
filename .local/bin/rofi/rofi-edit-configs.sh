#!/bin/bash
#
# RRRRRR   SSSSS   Richard Schembri
# RR   RR SS	   https://gitlab.com/richardschembri
# RRRRRR   SSSSS   https://github.com/richardschembri
# RR  RR       SS
# RR   RR  SSSSS
#
# Dmenu script for editing some of my more frequently edited config files.
# Taken from https://gitlab.com/dwt1/dotfiles/tree/master/.dmenu

declare options=("bash
alacritty
bspwm
dwm
polybar
st
sxhkd
vim
vifm
xmenu
zsh
quit")

sucklesspath="$HOME/AppSources/suckless"
choice=$(echo -e "${options[@]}" | rofi -mesg 'Edit a config file: ' -dmenu -i )

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	bash)
		choice="$HOME/.bashrc"
	;;
	bspwm)
		choice="$HOME/.config/bspwm/bspwmrc"
	;;
	dunst)
		choice="$HOME/.config/dunst/dunstrc"
	;;
	dwm)
		choice="$sucklesspath/dwm/patches/config.h"
	;;
	polybar)
		choice="$HOME/.config/polybar"
	;;
	alacritty)
		choice="$HOME/.config/alacritty/alacritty.yml"
	;;
	sxhkd)
		choice="$HOME/.config/sxhkd/sxhkdrc"
	;;
	vim)
		choice="$HOME/.vimrc"
	;;
	vifm)
		choice="$HOME/.config/vifm/vifmrc"
	;;
	xmenu)
		choice="$HOME/.local/bin/xmenu/xmenu-general.sh"
	;;
	zsh)
		choice="$HOME/.zshrc"
	;;
	*)
		exit 1
	;;
esac
$TERMINAL -e vim $choice
