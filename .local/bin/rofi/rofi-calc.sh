#!/bin/bash
#Requires rofi-calc to be installed
result=$(rofi -show calc -modi calc -no-show-match -no-sort | cut -d "=" -f2 | cut -d "≈" -f2 | sed -r 's/\s+//g') 

if [ -n "$result" ]; then
        echo "$result" | xclip -selection clipboard
        notify-send "'$result' copied to clipboard." &
fi
