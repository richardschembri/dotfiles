#!/bin/bash

MESSAGE="$1"
CANCEL_ICON=""
CONFIRM_ICON=""
CMD="$2"

case "$(echo -e "$CANCEL_ICON\n$CONFIRM_ICON" | rofi -dmenu -theme confirm_menu -mesg "$MESSAGE")" in
    $CANCEL_ICON$)  exit 1;;
    $CONFIRM_ICON) $CMD;;
esac

exit 1

