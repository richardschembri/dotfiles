#!/bin/bash

## Created By Aditya Shakya

rofi -modi run,drun -show drun -location 1 -xoffset 10 -yoffset 42 -line-padding 4 -columns 2 -width 40 -lines 10 -padding 25 -show-icons -drun-icon-theme "Papirus" -font "Misc Termsyn 16"
