#!/usr/bin/env nu

def main [
    --inc (-i)
    --dec (-d)
    --toggle (-t)
    --ismute (-m)
    --get (-g)
    --icon (-c)
    --asjson (-j)
] {
    mut audio_values = {
        step: 1
        muted: (pamixer --get-mute | into bool)
        volume: (pamixer --get-volume | into int)
        icons: {
            mute: ""
            low:  ""
            high: ""
        }
        icon: ""
    }

    if $audio_values.muted {
        $audio_values.icon = $audio_values.icons.mute
    } else if $audio_values.volume < 50 {
        $audio_values.icon = $audio_values.icons.low 
    } else {
        $audio_values.icon = $audio_values.icons.high
    }

    if $asjson {
        $audio_values | to json
    } else if $inc {
        pamixer -i $audio_values.step #$volumestep
    } else if $dec {
        pamixer -d $audio_values.step #$volumestep
    } else if $toggle {
        pamixer -t
    } else if $get {
        #pamixer --get-volume
        $audio_values.volume
    } else if $ismute {
        #pamixer --get-mute
        $audio_values.muted
    } else if $icon {
        $audio_values.audio
    }
}
