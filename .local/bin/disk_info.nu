#!/usr/bin/env nu

def main [
    --used (-u)
    --all (-a)
    --percent (-c)
    --asjson (-j)
] {
    let main_disk = (df -H | detect columns 
                         | insert Percent {|disk_values| $disk_values."Use%" | parse --regex '(?P<num>\d+)' | get 0.num}
                         | insert Used_GB {|disk_values| $disk_values."Used" | parse --regex '(?P<num>\d+)' | get 0.num}
                         | insert Size_GB {|disk_values| $disk_values."Size" | parse --regex '(?P<num>\d+)' | get 0.num}
                         | insert Parsed {|disk_values| $"($disk_values.Used_GB)/($disk_values.Size_GB) GB" }
                         | where Mounted == "/" | get 0)
    if $asjson {
        $main_disk | to json
    } else if $used {
        $main_disk.Used_GB
    } else if $all {
        $main_disk.Size_GB
    } else if $percent {
        $main_disk.Percent #"Use%" | parse --regex '(?P<num>\d+)' | get 0.num #| str substring --regex '\d+'
    }
}
