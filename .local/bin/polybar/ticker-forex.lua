#! /usr/bin/env lua

local http = require("socket.http")
local cjson = require("cjson")

-- https://theforexapi.com/
-- https://stackoverflow.com/questions/53830265/lua-curl-how-to-get-json-response

local api_url = "https://theforexapi.com/api/latest"
function ExchangeTicker(currencyFrom, currencyTo)
	-- https://theforexapi.com/api/latest?base=USD&symbols=EUR
	local requesrString = api_url .. "base=" .. currencyFrom .. "&symbols=" .. currencyTo
	local body, code = http.request(requesrString)
	local jsonDict = cjson.decode(body)

	return jsonDict.rates[1]
end

print(ExchangeTicker("USD", "EUR"))
