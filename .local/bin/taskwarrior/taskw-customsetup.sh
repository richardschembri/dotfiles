#!/bin/sh

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}

function generate_folder {
	NEW_FOLDER=$1
	echo "Folder $NEW_FOLDER is being generated..."
	if [ ! -d "$NEW_FOLDER" ]; then
		mkdir $NEW_FOLDER
		echo "Folder $NEW_FOLDER generated"
	else
		echo "Folder $NEW_FOLDER already exits"
fi
}

# Check if Config Name parameter has been passed
if [ -z "${1}" ]; then
    echo "Please enter a name for the config"
    exit 1
fi

CONFIG_NAME=$1

yes_or_no "Are you sure you want to create a taskwarrior configuration for $CONFIG_NAME?" || exit 1

# Generate custom .taskrc file
TEMPLATE_FILE=$HOME/.local/bin/taskwarrior/taskrc-template
CONFIG_FILE=$HOME/.taskrc-$CONFIG_NAME
# https://stackoverflow.com/questions/6214743/create-new-file-from-templates-with-bash-script
echo "Config file ${CONFIG_FILE} is being generated..."
if [ ! -f "$CONFIG_FILE" ]; then
	if [ -f "$TEMPLATE_FILE" ]; then
		sed -e "s/%CONFIG_NAME%/$CONFIG_NAME/g" $TEMPLATE_FILE > $CONFIG_FILE
	echo "Config file ${CONFIG_FILE} generated!"
	else
		echo "ERROR: Template file ${TEMPLATE_FILE} does not exist!"
	fi
else
	echo "Config file ${CONFIG_FILE} already exists"
fi

# Generate custom .task folder
TASK_FOLDER=$HOME/.task-$CONFIG_NAME
generate_folder "$HOME/.task-$CONFIG_NAME"
TASK_FOLDER_HOOKS=$TASK_FOLDER/hooks
generate_folder "$TASK_FOLDER_HOOKS"

if  yes_or_no "Do you want to add a timewarrior hook for $CONFIG_NAME?"
then
	TIMEHOOK=/usr/share/timewarrior/ext/on-modify.timewarrior
	if [ -f "$TIMEHOOK" ]; then
		cp $TIMEHOOK $TASK_FOLDER_HOOKS/
		chmod +x $TASK_FOLDER_HOOKS/on-modify.timewarrior
		echo "Timewarrior hook generated"
	else
		echo "$TIMEHOOK was not found. Unable to generate Timewarrior hook"
	fi
else
	echo "Skipping"
fi

echo "Add the following alias for each of use"
echo "alias task$CONFIG_NAME=\"task rc:$CONFIG_FILE\""


