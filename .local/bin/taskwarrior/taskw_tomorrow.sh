#!/usr/bin/env sh

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}

function show_usage (){
	printf "Usage: [options [parameters]]\n"
	printf "\n"
	printf "Options:\n"
	printf "-r|--rc, <path-to-alternate-file> ...\n"
	printf "         Specifies an alternate configuration file\n"
	printf "-t|--taskid, Task ID to get project and tags\n"
	printf "-h|--help, Print help\n"
return 0
}

RC=""
TASKID=""
MYPROJECT=""

# Parse the arguments passed to this script
while [ ! -z "$1" ]; do
	case "$1" in
		-h|--help)
		 show_usage
		 ;;
		-r|--rc)
		 shift
		 $RC=$1
		 ;;
		-t|--taskid)
		 shift
		 $TASKID=$1
		 ;;
		*)
		 echo "Incorrect input provided"
		 show_usage
	esac
done

# Script starts here
while yes_or_no "Add new task?"; do
	task proj
	if yes_or_no "Set new project name?"; then
		while [ -z "$MYPROJECT" ]
		do
			read -p "Enter project name: " MYPROJECT
		done
	fi

	read -p "Enter task: " mytask
	if [ -z "$MYPROJECT" ]; then
		task add proj:$MYPROJECT due:tomorrow $MYTASK
	else
		task modify $TASKID proj:$MYPROJECT due:tomorrow $MYTASK
	fi
done

