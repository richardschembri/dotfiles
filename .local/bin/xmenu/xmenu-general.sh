#!/bin/sh

cat <<EOF | xmenu | sh &
Launcher	~/.local/bin/rofi/rofi_launcher.sh
Files	pcmanfm
Browser
	Brave	brave-browser
	Firefox	firefox
	Chromium	chromium

Tools	
	Terminal	$TERMINAL
	Task Manager	$TERMINAL htop
	YaST
		YaST GUI	$TERMINAL su - -c "/sbin/yast2"
		YaST CLI	$TERMINAL -e sudo yast2
		Software	$TERMINAL su - -c "/sbin/yast2 sw_single"
	Display
		Monitor	arand
		Background	nitrogen
Office
	Calculator	~/.local/bin/rofi/rofi-calc.sh
	Document	libreoffice --writer
	Spreadsheet	libreoffice --calc
	Presentation	libreoffice --impress
	Draw	libreoffice --draw
Development
	IDE(VSCode)	code
	Unity	~/Applications/UnityHub.AppImage
Graphics
	Raster Graphics Editor(Gimp)	gimp
	Vector Graphics Editor(Inkscape)	inkscape
	3D Graphics Editor(Blender)	blender
	Voxel Editor (Magica Voxel)	wine ~/.local/bin/wine/MagicaVoxel/MagicaVoxel.exe
Multimedia
	Video Editor(kdenlive)	kdenlive
	Video Player(VLC)	vlc
Entertainment
	Steam	steam
Study
	Flash Cards	anki
Camera
	Toggle On	camtoggle
EOF
