#!/bin/sh

cat <<EOF | xmenu | sh &
To file
	Selected Area	screenshot -t 0 -s 0
	Current Window	screenshot -t 0 -s 1
	Full Screen	screenshot -t 0 -s 2

To clipboard
	Selected Area	screenshot -t 1 -s 0
	Current Window	screenshot -t 1 -s 1
	Full Screen	screenshot -t 1 -s 2

Open Screenshot folder	$FILE_GUI $HOME/Pictures/Screenshots

EOF
