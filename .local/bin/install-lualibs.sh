#!/bin/sh
#
# RRRRRR   SSSSS   Richard Schembri
# RR   RR SS	   https://gitlab.com/richardschembri
# RRRRRR   SSSSS   https://github.com/richardschembri
# RR  RR       SS
# RR   RR  SSSSS
#
# Install Lua libraries used for my lua scripts.

# Lua header files
sudo zypper in lua54-devel

# Curl functionality
sudo zypper in libcurl-devel
sudo luarocks install lua-curl # require "cURL"

# JSON functionality
sudo zypper in gcc-c++
sudo luarocks install rapidjson

# CLI argument functionality
sudo luarocks install lua_cliargs
