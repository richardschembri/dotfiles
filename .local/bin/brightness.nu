#!/usr/bin/env nu

def main [
    --inc (-i)
    --dec (-d)
    --dim (-m)
    --get (-g)
] {
    let step = 1
    let has_light = not (which light | is-empty)
    let has_xbacklight = not (which xbacklight | is-empty)

    if $inc {
        if $has_light {
            light -A $step
        } else if $has_xbacklight {
            xbacklight -inc
        }
    } else if $dec {
        if $has_light {
            light -U $step
        } else if $has_xbacklight {
            xbacklight -dec
        }
    } else if $dim {
        if $has_light {
            light -S 20
        } else if $has_xbacklight {
            xbacklight -set 20
        }
    } else if $get {
        if $has_light {
            light -G | into float | math round --precision 0
        } else if $has_xbacklight {
            xbacklight -get | into float | math round --precision 0
        }
    }
}
