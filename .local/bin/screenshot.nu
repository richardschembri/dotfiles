#!/usr/bin/env nu

def main [ ] { }

let folderpath_target = $"($env.HOME)/Pictures/Screenshots/"

def countdown_to [seconds: int] {
	for s in ($seconds)..1 {
		dunstify -t 1000 --replace=699 $"Taking shot in : ($s)"
		sleep 1sec
	}
}

# Capture to filepath
def "main file" [
		area: int = 0 # 0: Selection, 1: Current Window, 2: Full Screen
		--folderpath (-p): string # Which folder the screenshot will be saved 
		--countdown_seconds (-c): int = 0
	] {
	let filename = $"(date now | format date "%y%m%d-%H%M-%S").png"
	if ($folderpath != null) {
		let folderpath_target = $folderpath
	}

	if ($countdown_seconds > 0) {
		countdown_to $countdown_seconds
	}
	
	match $area {
		0 => (
			maim -s $"($folderpath_target)/pic-selected-($filename)"
		),
		1 => (
			maim -i $"(xdotool getactivewindow)" $"($folderpath_target)/pic-window-($filename)"
		),
		2 => (
			maim $"($folderpath_target)/pic-full-($filename)"
		)
	}
	# notify-send $"Screenshot captured to ($filepath)"
	notify-send $"Screenshot captured to ($folderpath_target)"
}

# Capture to clipboard
def "main clipboard" [
		area: int = 0 # 0: Selection, 1: Current Window, 3: Full Screen
		--countdown_seconds (-c): int = 0
	] {

	if ($countdown_seconds > 0) {
		countdown_to $countdown_seconds
	}

	match $area {
		0 => (
			maim -s | xclip -selection clipboard -t image/png
		),
		1 => (
			maim -i $"(xdotool getactivewindow)" | xclip -selection clipboard -t image/png
		),
		2 => (
			maim | xclip -selection clipboard -t image/png
		)
	}
	notify-send "Screenshot captured to clipboard"
}

def "main printpath" [] {
	$folderpath_target	
}

# Print most recently saved screenshot
def "main printrecent" [
		area: int = 0 # 0: Selection, 1: Current Window, 3: Full Screen
		--folderpath (-p): string
	] {
	let screenshot_type = ""
	if ($folderpath != null) {
		let folderpath_target = $folderpath
	}
	cd $folderpath_target

	match $area {
		0 => (
			let screenshot_type = "pic-selected-"
		),
		1 => (
			let screenshot_type = "pic-window-"
		),
		2 => (
			let screenshot_type = "pic-full-"
		)
	}
	print (ls | where name =~ $"($screenshot_type)" | sort-by modified --reverse | get 0.name)
}

# Print most recently saved screenshot
def "main openrecent" [
		area: int = 0 # 0: Selection, 1: Current Window, 3: Full Screen
		--folderpath (-p): string
	] {
	sxiv (screenshot.nu printrecent $area --folderpath $folderpath)
}
