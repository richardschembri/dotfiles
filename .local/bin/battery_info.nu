#!/usr/bin/env nu

def main [
    --asjson (-j)
    --icon (-i)
    --time (-t)
    --percentage (-p)
] {

    let level_all = (acpi -b | rg -v "unavailable" | rg -o "[0-9][0-9]?[0-9]?%")
    let level = ($level_all | awk -F"%" 'BEGIN{tot=0;i=0} {i++; tot+=$1} END{printf("%d%%\n", tot/i)}')
    # Declare json
    mut bat_values = { 
        level_all: ($level_all | sed 's/[^0-9]//g' | into int)
        level: ($level | sed 's/[^0-9]//g' | into int)
        time: (acpi -b | awk '{printf $5}' | str substring 0..5)
        charging: (acpi -b | rg -w 0: | rg -c Discharging | is-empty)
        icons: {
            charging: ""
            empty: ""
            quarter: ""
            half: ""
            threequarter: ""
            full: ""
        }
        level_icon: ""
    }

    if ($bat_values.charging) {
        $bat_values.level_icon = $bat_values.icons.charging
    } else if ($bat_values.level < 15) {
        $bat_values.level_icon = $bat_values.icons.empty
    } else if ($bat_values.level <= 25) {
        $bat_values.level_icon = $bat_values.icons.quarter
    } else if ($bat_values.level <= 50) {
        $bat_values.level_icon = $bat_values.icons.half
    } else if ($bat_values.level <= 75) {
        $bat_values.level_icon = $bat_values.icons.threequarter
    } else {
        $bat_values.level_icon = $bat_values.icons.full
    }

    if $asjson {
        $bat_values | to json
    } else if $percentage {
        $bat_values.level
    } else if $time {
        $bat_values.time
    } else if $icon {
        $bat_values.level_icon
    }

}
