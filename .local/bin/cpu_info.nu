#!/usr/bin/env nu

# Inspired from:
#  awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){u1=u; t1=t;} else print ($2+$4-u1) * 100 / (t-t1) "%"; }' \
# <(grep 'cpu ' /proc/stat) <(sleep 1;grep 'cpu ' /proc/stat)

def getcpu_stats [] {
    (open --raw /proc/stat | from ssv --noheaders --minimum-spaces 1 | first 1 
     | select column1 column2 column3 column4 column5 column6 column7 column8
     | rename label user nice system idle iowait irq softirq | into value
#     | insert mode_user {|stats| ($stats.user + $stats.system)}
#     | insert mode_system {|stats| ($stats.user + $stats.system + $stats.idle)}
     | insert cpu_sum {|stats| ($stats.user + $stats.nice + $stats.system + $stats.idle + $stats.iowait + $stats.irq + $stats.softirq)}
     | get 0
    )
}

def getcpu_usage [] {
    let cpu_prev = getcpu_stats
    sleep 1sec
    let cpu_now = getcpu_stats

    let cpu_delta = $cpu_now.cpu_sum - $cpu_prev.cpu_sum
    let cpu_idle = $cpu_now.idle - $cpu_prev.idle

    let cpu_used = $cpu_delta - $cpu_idle
    $cpu_used * 100 / $cpu_delta | math round --precision 0
}

def getcpu_temp [] {
    mut temp = {
        celsius: (((open /sys/class/thermal/thermal_zone0/temp) | into float) / 1000)
        icons: {
            low: ""
            high: ""
        }
        temp_icon: ""
    }

    if ($temp.celsius < 80) {
        $temp.temp_icon = $temp.icons.low
    } else {
        $temp.temp_icon = $temp.icons.high
    }
    $temp
}

def main [
    --percent (-p)
    --temp (-t)
    --asjson (-j)
] {
    if ($percent) {
        getcpu_usage
    } else if ($temp) {
        (getcpu_temp).celsius
    } else if ($asjson) {
        let cpu_values = {
            usage: (getcpu_usage)
            temp: (getcpu_temp)
        }
        $cpu_values | to json
    }
}
