package.cpath = package.cpath .. ";/usr/lib/lua/5.4/?.so"
local json = require "rapidjson"
local curl = require "cURL"

local api_url_kraken = "https://api.kraken.com/0/public/Ticker"

tickercurrency = {}

function tickercurrency.GetTickerKraken(currFrom, currTo)
	-- args = user_args or {}
	currFrom = currFrom or "BTC"
	currTo = currTo or "EUR"

	apiresponse = {}
	requestUrl = api_url_kraken .. "?pair=" .. currFrom  .. currTo
	c = curl.easy{
		url            = requestUrl,
		writefunction  = function(str)
			apiresponse = json.decode(str)
		end
	}
	:perform()
	:close()
	if(next(apiresponse["error"])) then
		return "-1"
	end
	-- return string.format( "%.2f", apiresponse["result"][currFrom  .. currTo]["c"][1])
	for key, value in pairs(apiresponse["result"]) do
		return string.format( "%.2f", value["c"][1])
	end
end

return tickercurrency
