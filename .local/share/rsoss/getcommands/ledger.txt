  _             _
 | |    ___  __| | __ _  ___ _ __
 | |   / _ \/ _` |/ _` |/ _ \ '__|
 | |__|  __/ (_| | (_| |  __/ |
 |_____\___|\__,_|\__, |\___|_|
                  |___/

zathura is the pdf/djvu reader.
	# Get balance
	ledger -f 1.ledger bal
	# Get balance in GBP
	ledger -f 1.ledger bal -X GBP
