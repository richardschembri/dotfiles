 ______     __         ______     __   __     _____     ______     ______    
/\  == \   /\ \       /\  ___\   /\ "-.\ \   /\  __-.  /\  ___\   /\  == \   
\ \  __<   \ \ \____  \ \  __\   \ \ \-.  \  \ \ \/\ \ \ \  __\   \ \  __<   
 \ \_____\  \ \_____\  \ \_____\  \ \_\\"\_\  \ \____-  \ \_____\  \ \_\ \_\ 
  \/_____/   \/_____/   \/_____/   \/_/ \/_/   \/____/   \/_____/   \/_/ /_/ 

Blender is a 3D modeling software.
	SELECTION
		a - select [a]ll
		c - [c]ircle select
		b - [b]ox select
		alt + right click - loop select
	tab - toggle edit/object mode
	g - grab mode (x,y,z to lock to corresponding axis)
	z - wireframe mode
	a - deselect
	b - [b]order select // select multiple vertices
	s - [s]cale mode (x,  y, z) depending on axis
	shift + click - grab multiple vertices (1 by 1)
	e - [e]xtrude
	x- delete mode
	ctrl + r - loop cut (scrolling adds more)
	r - [r]otate
	w - merge
	k - [k]nife, subdivides faces
	t - toggle [t]ools (left side)
	n - toggle properties (right side)
	d - [d]uplicate
	p - se[p]erate
	ctrl+j - [j]oin objects
	ctrl+l - select all [l]inked vertices
