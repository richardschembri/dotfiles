#!/bin/bash

brightness=$(expr $(xbacklight -get) - $1)

if [ $brightness -lt 5 ]
then
	brightness=5
fi

xbacklight -set $brightness
