#!/bin/bash
#
# RRRRRR   SSSSS   Richard Schembri
# RR   RR SS	   https://gitlab.com/richardschembri
# RRRRRR   SSSSS   https://github.com/richardschembri
# RR  RR       SS
# RR   RR  SSSSS
#
# Dmenu script for editing some of my more frequently edited config files.
# Taken from https://gitlab.com/dwt1/dotfiles/tree/master/.dmenu

declare options=("bash
bspwm
dwm
polybar
st
sxhkd
vim
vifm
zsh
quit")

sucklesspath="$HOME/AppSources/suckless"
choice=$(echo -e "${options[@]}" | dmenu -i -p 'Edit a config file: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	bash)
		choice="$HOME/.bashrc"
	;;
	bspwm)
		choice="$HOME/.config/bspwm/bspwmrc"
	;;
	dwm)
		choice="$sucklesspath/dwm/patches/config.h"
	;;
	polybar)
		choice="$HOME/.config/polybar"
	;;
	st)
		choice="$sucklesspath/st/patches/config.h"
	;;
	sxhkd)
		choice="$HOME/.config/sxhkd/sxhkdrc"
	;;
	vim)
		choice="$HOME/.vimrc"
	;;
	vifm)
		choice="$HOME/.config/vifm/vifmrc"
	;;
	zsh)
		choice="$HOME/.zshrc"
	;;
	*)
		exit 1
	;;
esac
st vim $choice
