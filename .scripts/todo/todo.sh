#!/bin/bash

function gettodo_date(){
  detectdays=$1
  echo $(date -d "-${detectdays} days" +"%Y-%m-%d")
}

function gettodo_folderpath(){
  todo_date=$(gettodo_date $1)
  todo_yr=$(date -d "${todo_date}" +"%Y")
  todo_mon=$(LANG=en_us_88591; date -d "${todo_date}" +"%b")
  echo "$todo_yr/$todo_mon/"
}

function gettodo_filepath(){
  todo_date=$(gettodo_date $1)
  todo_folderpath=$(gettodo_folderpath $1)
  todo_ymd=$(date -d "${todo_date}" +"%Y%m%d")
  todo_filename="todo$todo_ymd.md"
  echo "$todo_folderpath$todo_filename"
}

todo_path=$(gettodo_folderpath 0)
todo_fullpath=$(gettodo_filepath 0)

if [ ! -f $todo_fullpath ]; then
  if [ ! -d $todo_path ]; then
    mkdir -p $todo_path 
  fi
  for i in {1..356}
  do
    prevtodo_filepath=$(gettodo_filepath $i)
    if [ -f $prevtodo_filepath ]; then
      cp $prevtodo_filepath $todo_fullpath  
      break
    fi
  done

  if [ ! -f $todo_fullpath ]; then
    cp todo-template.md $todo_fullpath  
  fi

fi

if hash taskell 2>/dev/null; then
  taskell $todo_fullpath
else
  vim $todo_fullpath
fi
