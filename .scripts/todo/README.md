# TODO List manager script

A simple script designed to create and/or open a todo list for the current
day. 

The todo list is an md file with the format that works with [taskell][1].
If **taskell** is not installed it will open the file with **vim**. 

The script tries to copy the latest todo list within a 356 day period. If
it doesn't find anything it will copy from a template.

[1][https://github.com/smallhadroncollider/taskell]
