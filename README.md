# RS dotfiles
These are my dotfiles, designed to work on Linux, MacOS and WSL.
Where possible, applications are configured to use VIM-like keybindings.
## Scripts
- Most scripts are located in `~/.local/bin`
- The path is exported in `~/.profile`
## Unix Shell
- **Text Editor**: vim
- **TUI File Manager**: vifm
- **TODO List**: taskwarrior
- **RSS Reader**: newsboat
## Cross Platform
- **Terminal**: Alacritty
## Linux Only
> Currently targeted for OpenSUSE
### Desktop Enviroment
A custom minimal desktop enviroment composed of the following applications:
- **Window Manager (Main):** bspwm
- **Window Manager (Alternate):** openbox
- **Menu System:** xmenu
	- **xmenu-general**: *Acts as the start menu for linux*
	- **xmenu-screenshot**: *A menu for various types of screenshot options, like snippets and the option to capture to clipboard or to file.*
- **Window switcher + Application launcher**: rofi
- **Status/Task bar**: polybar
- **Hotkey manager**: sxhkd
- **Compositor**: picom
- **GUI File Manager**: pcmanfm
- **Notification System**: dunst
