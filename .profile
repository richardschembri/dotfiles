# export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"

# export EDITOR=/usr/bin/nvim
# export TERMINAL=alacritty
# export READER=zathura
# export FILE=vifm
# export BROWSER=brave-browser

# export SUDO_ASKPASS="$HOME/.scripts/dmenu/dmenu-pass.sh"

# export INPUT_METHOD="fcitx"
# export SHELL=$(which zsh)

# command -v bat >/dev/null && export MANPAGER="sh -c 'col -bx | bat -l man -p'" && export MANROFFOPT="-c"

# [ -d "$HOME/go/bin" ] && export PATH="$PATH:$(du "$HOME/go/bin/" | cut -f2 | paste -sd ':')"
# [ -d "$HOME/.cargo/bin" ] && path+=$HOME/.cargo/bin
# [ -f "$HOME/.cargo/env" ] && source "$HOME/.cargo/env"
# [ -f "$HOME/.ruby/" ] && export GEM_HOME="$HOME/.ruby/"
# [ -f "$HOME/.ruby/bin" ] && export PATH="$PATH:$HOME/.ruby/bin"

[ -z "$OSNAME" ] && [ -f "$HOME/.config/.osrc" ] && source "$HOME/.config/.osrc"
[[ ! $CUSTOMALIAS = true ]] && [ -f "$HOME/.config/.aliasrc" ] && source "$HOME/.config/.aliasrc"
[[ ! $CUSTOMFUNCTION = true ]] && [ -f "$HOME/.config/.functionrc" ] && source "$HOME/.config/.functionrc"
